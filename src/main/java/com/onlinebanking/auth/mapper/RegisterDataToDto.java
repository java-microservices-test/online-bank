package com.onlinebanking.auth.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinebanking.auth.dto.RegisterRequest;
import com.onlinebanking.auth.dto.RegisterResponse;

import java.util.List;

public class RegisterDataToDto {
        public RegisterRequest RegisteServiceToDto(RegisterRequest registerRequest) throws JsonProcessingException {
                ObjectMapper mapper = new ObjectMapper();
                String json = mapper.writeValueAsString(registerRequest);
                RegisterResponse registerResponse1 = mapper.readValue(json, RegisterResponse.class);;
                registerRequest.setFullname(registerResponse1.getFullname());
                registerRequest.setPhone(registerResponse1.getFullname());
                registerRequest.setEmail(registerResponse1.getEmail());
                registerRequest.setLogin(registerResponse1.getLogin());
                registerRequest.setPassword(registerResponse1.getPassword());
                registerRequest.setPhone(registerResponse1.getPhone());
                return registerRequest;
        };



}
