package com.onlinebanking.auth.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.onlinebanking.auth.dto.RegisterRequest;
import com.onlinebanking.auth.dto.RegisterResponse;

import java.util.List;

public interface RegisterService {
    void registerUser(String s) throws JsonProcessingException;

    void registerUser(RegisterResponse registerResponse) throws JsonProcessingException;

  /*  boolean registerUser(RegisterRequest registerRequest) throws JsonProcessingException;*/

    boolean registerUser(String login, String password, String email, String phone, String fullname) throws JsonProcessingException;

    RegisterResponse registerUser(RegisterRequest registerRequest) throws JsonProcessingException;

    /*boolean registerUser(List<String>) throws JsonProcessingException;*/
}
