package com.onlinebanking.auth.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.onlinebanking.auth.dto.RegisterRequest;
import com.onlinebanking.auth.dto.RegisterResponse;
import com.onlinebanking.auth.service.FeignRequestService;
import com.onlinebanking.auth.service.RegisterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class RegisterServiceImpl implements RegisterService {
    private static final Logger log = LoggerFactory.getLogger(RegisterServiceImpl.class);
    @Autowired
    private FeignRequestService feignRequestService;

    @Override
    public void registerUser(String s) throws JsonProcessingException {

    }

    @Override
    public void registerUser(RegisterResponse registerResponse) throws JsonProcessingException {

    }

    @Override
    public boolean registerUser(String login, String password, String email, String phone, String fullname) throws JsonProcessingException {
        return false;
    }

    @Override
    public RegisterResponse registerUser(RegisterRequest registerRequest) throws JsonProcessingException {
        RegisterResponse registerResponse = new RegisterResponse();
        registerRequest = buildRegisterReuest(registerRequest);
        log.info("/api/v1/checkregistration {}", registerRequest);
        feignRequestService.checkReg(registerRequest);
        return registerResponse;
    }

    private RegisterRequest buildRegisterReuest(RegisterRequest registerRequest){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        RegisterRequest request = new RegisterRequest();
        request.setLogin(registerRequest.getLogin());
        request.setPassword(bCryptPasswordEncoder.encode(registerRequest.getPassword()));
        request.setFullname(registerRequest.getFullname());
        request.setEmail(registerRequest.getEmail());
        request.setPhone(registerRequest.getPhone());

        return request;
    }
    public String encodePassword(String password){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String hashedPassword = encoder.encode(password);
        return hashedPassword;
    }



}
