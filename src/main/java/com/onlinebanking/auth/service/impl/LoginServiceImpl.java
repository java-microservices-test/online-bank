package com.onlinebanking.auth.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.onlinebanking.auth.dto.LoginRequest;
import com.onlinebanking.auth.dto.LoginResponse;
import com.onlinebanking.auth.dto.RegisterRequest;
import com.onlinebanking.auth.service.FeignRequestService;
import com.onlinebanking.auth.service.LoginService;
import com.onlinebanking.auth.service.RegisterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    FeignRequestService feignRequestService;

    private static final Logger log = LoggerFactory.getLogger(RegisterServiceImpl.class);
    public LoginResponse loginUser(LoginRequest loginRequest){
        LoginResponse loginResponse = new LoginResponse();
        loginRequest = buildLoginRequest(loginRequest);
        log.info("POST/api/v1/login/");
        feignRequestService.loginUser(loginRequest);
        String encodedPasswordFromDb="";
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        boolean passwordsMatch = encoder.matches(loginRequest.getPassword(),encodedPasswordFromDb);

        return loginResponse;

    }
    private LoginRequest buildLoginRequest(LoginRequest loginRequest){
        LoginResponse loginResponse = new LoginResponse();
        LoginRequest request = new LoginRequest();
        request.setLogin(loginRequest.getLogin());
        request.setPassword(loginRequest.getPassword());
        return request;
    }
    public String encodePassword(String password){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String hashedPassword = encoder.encode(password);
        return hashedPassword;
    }
}
