package com.onlinebanking.auth.service;

import com.onlinebanking.auth.dto.LoginRequest;
import com.onlinebanking.auth.dto.LoginResponse;
import com.onlinebanking.auth.dto.RegisterRequest;
import com.onlinebanking.auth.dto.RegisterResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "wallet-user-service", url = "http://127.0.0.1:8081")
public interface FeignRequestService {
    @PostMapping ("/api/v1/users/checkregistration/")
    RegisterResponse checkReg(@RequestBody RegisterRequest registerRequest);
    @PostMapping("/api/v1/users/save/")
    RegisterResponse saveUser(@RequestBody RegisterRequest registerRequest);
    @PostMapping("/api/v1/login/check/")
    Boolean loginUser(@RequestBody LoginRequest loginRequest);



}
