package com.onlinebanking.auth.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.onlinebanking.auth.dto.LoginRequest;
import com.onlinebanking.auth.dto.RegisterRequest;
import com.onlinebanking.auth.dto.RegisterResponse;
import com.onlinebanking.auth.service.impl.LoginServiceImpl;
import com.onlinebanking.auth.service.impl.RegisterServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
public class AuthController {
    private static final Logger log = LoggerFactory.getLogger(AuthController.class);
    @Autowired
    private RegisterServiceImpl registerService;
    @Autowired
    private LoginServiceImpl loginService;

    @PostMapping("/register/")
    public void createClient(@RequestBody RegisterRequest registerRequest) throws JsonProcessingException {
        log.debug("execute check registration for user {}");
        registerService.registerUser(registerRequest);
    }
    @PostMapping("/login/")
    public void loginClient(@RequestBody LoginRequest loginRequest){
        log.debug("login user start {}");
        loginService.loginUser(loginRequest);
    }
}
