package com.onlinebanking.auth.dto;

import lombok.Data;

@Data
public class LoginResponse {
    private String login;
    private String password;
}
